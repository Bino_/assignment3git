#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		ofVideoGrabber cam;
		ofImage frame;
		int colorTime;

		ofxPanel gui;
		ofxIntSlider hue;
		ofxIntSlider saturation;
};
