#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	cam.setup(1280, 720); 
	frame.allocate(1280, 720, OF_IMAGE_COLOR);
	colorTime = 4;
	gui.setup();
	gui.add(hue.setup("Hue", 80, 20, 100));
	gui.add(saturation.setup("Saturation", 200, 0, 400));
}

//--------------------------------------------------------------
void ofApp::update(){
	if (cam.isInitialized()) { //make sure camera is ready
		cam.update(); //get a new frame from the camera
		if (cam.isFrameNew()) { //make sure we have a new camera
			frame.setFromPixels(cam.getPixels()); //copy the camera info to our frame holder
		}
	}
	//printf("%d\n", colorTime);
	if (colorTime == 20 || colorTime == -50) {
		colorTime *= -1;
	}
	colorTime++;
}

//--------------------------------------------------------------
void ofApp::draw(){
	
	for (int x = 0 ; x < 1280; x += 15) { //go thru the entire frame, 15 pixels at a time
		for (int y = 0; y < 720; y += 15) {
					ofColor pixel = frame.getColor(x, y); //grab the color of the pixel at this location
					pixel.setSaturation(saturation);  //pump up the saturation!
					pixel.setHue(pixel.getHue() + hue ); //shift the hue
					ofSetColor(pixel);
					float bright = pixel.getBrightness(); //calculate the brightness of the pixel
					float hue = pixel.getHue();
					//ofSetColor(bright);
					//ofNoFill();
					ofDrawRectangle(x, y, hue / 2, hue / 2);
					ofDrawRectangle(x + 1280, y, hue / 2, hue / 2);
					ofDrawRectangle(x, y + 720, hue / 2, hue / 2);
					ofDrawRectangle(x + 1280, y + 720, hue / 2, hue / 2);
		}
	}
	gui.draw();
}
